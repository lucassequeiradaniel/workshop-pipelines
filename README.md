# workshop-pipelines

## Temario

- [ ] Que es un pipeline
- [ ] Crear un pipeline
- [ ] Que es un stage
- [ ] Crear un stage
- [ ] Que es un job
- [ ] Crear un job
- [ ] before_script y after_script
- [ ] Extender jobs
- [ ] Scripting en un job
- [ ] Variables de entorno. Global y Local
- [ ] Detectar entornos con rules
- [ ] Cambiar comportamiento con rules
- [ ] Artifacts y cache
- [ ] Extender un pipeline
